//
//  VDLMainViewController.m
//  Dropin-Player
//
//  Created by Felix Paul Kühne on 19.11.13.
//  Copyright (c) 2013 VideoLAN. All rights reserved.
//

#import "VDLMainViewController.h"
#import "VDLAppDelegate.h"

@interface VDLMainViewController ()

@end

@implementation VDLMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)example1:(id)sender
{
    VDLAppDelegate *appDelegate = (VDLAppDelegate *)[UIApplication sharedApplication].delegate;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"example1" ofType:@"webm"];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat: @"file://%@", path]];
    [appDelegate playStreamFromURL: url];
}

- (IBAction)example2:(id)sender
{
    VDLAppDelegate *appDelegate = (VDLAppDelegate *)[UIApplication sharedApplication].delegate;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"example2" ofType:@"webm"];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat: @"file://%@", path]];
    [appDelegate playStreamFromURL: url];
}

- (IBAction)example3:(id)sender
{
    VDLAppDelegate *appDelegate = (VDLAppDelegate *)[UIApplication sharedApplication].delegate;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"example3" ofType:@"webm"];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat: @"file://%@", path]];
    [appDelegate playStreamFromURL: url];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
